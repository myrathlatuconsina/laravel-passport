<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CommandLogin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feature:login';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Passport login via command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Feature login...');
    }
}
